class CreatePreferencesAndAddCurrentPrefToUser < ActiveRecord::Migration
  def change
    create_table :preferences do |t|
      t.belongs_to :user, index: true
      t.string :name
      t.string :address
      t.string :country
      t.integer :dist_max
      t.integer :games_max
      t.integer :days_max
      t.time :time_min
      t.time :time_max
      t.float :longitude
      t.float :latitude

      t.timestamps null: false
    end
    add_column :users, :current_pref_id, :integer
  end
end
