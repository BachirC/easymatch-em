class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
    	t.belongs_to :owner, index: true
      t.text :description
      t.string :address
      t.float :longitude
      t.float :latitude
      t.date :date
      t.time :time
      t.integer :sport

      t.timestamps null: false
    end
    
    create_table :games_participants, id: false do |t|
      t.belongs_to :game, index: true
      t.belongs_to :user, index: true


      t.timestamps null: false
    end
  end
end
