$(document).on("page:change", function(){

	$(".pref-selector").click(function(){

		$(".pref-table").find("tr").removeClass("selected");
		pref_id = $(this).data("id");
		$(this).addClass("selected");
		$.post("preferences/"+pref_id+"/select", {id: pref_id}, function(data){});
	});

});