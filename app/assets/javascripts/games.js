$(document).on("page:change", function(){
	$('a#hide-button').click(function(event){
		link_b = $(this);
		if (link_b.text() == "Hide Map") {
			link_b.text("Show Map");
		} else {
			link_b.text("Hide Map");
		};
		event.preventDefault();
		button_hide = $('div#map-toggle');
		$('div#map-toggle').toggle();
	});

	$(".game-selector").click(function(){
		game_id = $(this).data("id");
		$.get("games/"+game_id+"/get_info", function(data){
		});
	});
});
