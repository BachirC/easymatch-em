json.array!(@games) do |game|
  json.extract! game, :id, :owner, :address, :participants, :date, :time, :sport
  json.url game_url(game, format: :json)
end
