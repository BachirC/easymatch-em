json.array!(@preferences) do |preference|
  json.extract! preference, :id, :address, :country, :dist_max, :games_max
  json.url preference_url(preference, format: :json)
end
