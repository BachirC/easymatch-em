class GamesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_game, except: [:new, :create, :index]

  # GET /games
  # GET /games.json
  def index
    @games = Game.upcoming.order("date").order("time").page(params[:page]).per(10)
    @markers = build_markers(@games) 
  end

  # GET /games/1
  # GET /games/1.json
  def show
    @marker = build_markers(@game)
  end

  # GET /games/new
  def new
    @game = Game.new
  end

  # GET /games/1/edit
  def edit
  end

  # POST /games
  # POST /games.json
  def create
    @game = Game.new(game_params)
    @game.owner = current_user 
    @game.participants << current_user 
    respond_to do |format|
      if @game.save
        format.html { redirect_to @game, notice: 'Game was successfully created.' }
        format.json { render :show, status: :created, location: @game }
      else
        format.html { render :new }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def update
    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to @game, notice: 'Game was successfully updated.' }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @game.destroy
    respond_to do |format|
      format.html { redirect_to games_url, notice: 'Game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # POST /games/:id/join
  def join
    @game.participants << current_user unless @game.participants.include? current_user
    redirect_to @game, notice: 'You joined the game !'
  end

  # POST /games/:id/leave
  def leave
    @game.participants.delete current_user if @game.participants.include? current_user
    redirect_to @game, notice: 'You left the game !'
  end

  def get_info
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
      @game = Game.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
      params.require(:game).permit(:owner_id, :address, :participants, :date, :time, :sport, :description, :country)
    end

    def build_markers(games)
      iconPin = 'https://s3-us-west-2.amazonaws.com/yespark-website-assets/parking-index/pin_bleu.png'
      Gmaps4rails.build_markers(games) do |game, marker|
        marker.lat game.latitude
        marker.lng game.longitude
        marker.picture({
          "url" => view_context.image_path("pin_#{game.sport}.png"),
          "width" => 32,
          "height" => 37
  })
        marker.infowindow "#{game.sport}</br>#{game.date.strftime("%d/%m/%Y")}</br>début : #{game.time.strftime("%H:%M")}</br><a href='/games/#{game.id}'>Détails</a>"
      end
    end
end
