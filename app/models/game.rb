class Game < ActiveRecord::Base

	SPORTS = ["Football", "Basketball", "Biking", "Running"]
	enum sport: SPORTS

	has_and_belongs_to_many :participants, class_name: User, join_table: "games_participants"
	belongs_to :owner, class_name: User, foreign_key: "owner_id"

	validates_presence_of :address, :date, :time, :sport, :country, :latitude, :longitude

	geocoded_by :location			   # can also be an IP address
	before_validation :geocode          # auto-fetch coordinates

	scope :upcoming, -> { where("date >= ?", Date.today) } 
	scope :archived, -> { where("date < ?", Date.today) }

	def location
		"#{address} #{::ISO3166::Country[country].name}" || nil
	end

end
