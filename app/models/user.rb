class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  	devise :database_authenticatable, :registerable,
        :recoverable, :rememberable, :trackable, :validatable

    has_and_belongs_to_many :games, join_table: "games_participants"
    has_many :preferences

    validates :username, uniqueness: true

    def pref_selected
      Preference.find(self.current_pref_id)
    end
    
    def email_required?
    	false
    end

    def email_changed?
    	false
    end
end
