class Preference < ActiveRecord::Base
	belongs_to :user

	geocoded_by :location			   # can also be an IP address
	before_validation :geocode          # auto-fetch coordinates

	validates_presence_of :name, :latitude, :address, :country, :dist_max
	validates_uniqueness_of :name

	def location
		"#{address} #{::ISO3166::Country[country].try(:name)}"
	end

	
end
